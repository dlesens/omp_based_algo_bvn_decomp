# OMP based algorithm for the BvN decomposition


Code for the paper "Orthogonal matching pursuit-based algorithms for the Birkhoff-von Neumann decomposition"

## Organization

The directory structure is as below.

```
.
├── bottled
├── bvn
│   ├── bottled_c.pyx
│   ├── greedy.py
│   └── setup.py
├── ex21.mtx
├── README.md
├── test.py
└── utils
    ├── ds_matrix.py
    └── scaling.py
```

Code in the bottled directory is a copy of the repository [Bottled](https://gitlab.inria.fr/bora-ucar/bottled)

## Building and Usage

- Our code is in Python and makes call to C functions throught Cython.

- Matchings are done in the C part while matrix manipulations and optimisation problems are solved in the Python part.

### Building

- To compile the project, run `python3 setup.py build_ext --inplace` in the /bvn repository.

### Usage

- Usecases are presented in the file `test.py` and we added the matrix `ex21` from the SuiteSparse matrix collection for you to test the code.

## Maximum weight matching

- For the experiments presented in the paper we used MC64 maximum weight matching Fortran implementation. Because this implementation is under license, we replaced it here with the [Networkx](https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.bipartite.matching.minimum_weight_full_matching.html) implementation.