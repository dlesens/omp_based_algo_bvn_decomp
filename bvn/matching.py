import networkx as nx

import numpy as np
from scipy.sparse import csc_array

def max_weight_matching(T):

    idi,idj = T.nonzero()
    n,_ = T.shape

    G = nx.Graph()
    G.add_nodes_from([i for i in range(2*n)])

    for k in range(len(idi)):
        G.add_edge(idi[k],idj[k]+n,weight=1-T[idi[k],idj[k]])
    
    M = nx.bipartite.minimum_weight_full_matching(G,top_nodes=list(range(n)))
    
    r = np.zeros(n,dtype=int)

    for (u,v) in list(M.items())[:n]:
        if u>v:
            u,v = v,u
        r[int(v-n)]=int(u)
    
    return r
