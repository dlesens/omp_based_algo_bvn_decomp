from setuptools import Extension, setup
from Cython.Build import cythonize
import numpy

# distutils: define_macros=NPY_NO_DEPRECATED_API=NPY_1_7_API_VERSION

extensions = [
    Extension("*", ["*.pyx"],
        include_dirs=[numpy.get_include(),'./../bottled/','./../bottled/extern/'],
        extra_compile_args=["-DMAIN_C -lgfortran -lm"],
        define_macros=[("NPY_NO_DEPRECATED_API", "NPY_1_7_API_VERSION")],
        extra_objects = ['./../bottled/mc64a.o'],
        #extra_link_args = ['./../bottled/mc64a.o'],
        libraries=['gfortran']),
]
setup(
    name="greedy",
    ext_modules=cythonize(extensions),
)
