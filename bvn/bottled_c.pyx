import numpy as np
from cpython.mem cimport PyMem_Malloc, PyMem_Free

cdef extern from "./../bottled/extern/matching.c":
    pass

cdef extern from "./../bottled/extern/cheap.c":
    pass

cdef extern from "./../bottled/bottleneckBipartiteMatching.c":
    pass

cdef extern from "./../bottled/matrixUtils.c":
    pass

cdef extern from "./../bottled/mmio.c":
    pass

cdef extern from "<stdio.h>":
    ctypedef struct FILE

cdef extern from "<float.h>":
    cdef const double DBL_MAX

cdef extern from "./../bottled/mmio.h":
    ctypedef char MM_typecode[4]
    int mm_read_banner(FILE *f, MM_typecode *matcode)
    int mm_read_mtx_crd_size(FILE *f, int *M, int *N, int *nz)
    int mm_read_mtx_array_size(FILE *f, int *M, int *N)
    int mm_write_banner(FILE *f, MM_typecode matcode)
    int mm_write_mtx_crd_size(FILE *f, int M, int N, int nz)
    int mm_write_mtx_array_size(FILE *f, int M, int N)

cdef extern from "./../bottled/matrixUtils.h":
    void randPermColumns(int *col_ptrs, int * col_ids, double *col_vals, int n)
    void randPermRows(int *col_ptrs, int * col_ids, double *col_vals, int n)
    void ScaleOnePattern(	int *col_ptrs, int *col_ids, double *col_vals, int n, int m, int numIters )
    void ScaleOne( int *col_ptrs, int *col_ids, double *col_vals, int n, int m, int numIters )
    double computeScalingError( int *col_ptrs, int *col_ids, double *col_vals, int n, int m )
    void ReadMatrixMarket( char *fname, double **A, int **csc_irn, int **csc_jcn,int *_m, int *_n, int *_ne, int readVals )

cdef extern from "./../bottled/portdefs.h":
    void transpose(int* sptrs, int* sids, double *svals, int* tptrs, int* tids, double *tvals, int n, int m)
    int sprank(int *col_ptrs, int *col_ids,  int n, int m, int *tmpspace)
    void shuffle(int *a, int n)
    void bttlThresholdInitializer(int *col_ptrs, int *col_ids, double *col_vals, int n, int m, int nz, 
        int *row_ptrs, int *row_ids, double *row_vals,
        int *fend_cols, int *fend_rows,
        double *thrshld_g, int maxcrdmatch)
    int bttlThreshold(int *col_ptrs, int *col_ids, double *col_vals, int n, int m, int *match, int *row_match, 
        int *row_ptrs, 
        int *row_ids,
        double *row_vals,
        int *fend_cols, int *fend_rows,
        int lbapAlone, double *thrshld_g, int sprankknown)
    int bisectionBasedOnMC64J3(int *col_ptrs, int *col_ids, double *col_vals, int n, int m, int *match, int *row_match, 
        int *row_ptrs, 
        int *row_ids,
        double *row_vals,
        int *fend_cols, int *fend_rows, double *thrshld, int sprankknown)
    int pureSAP(int *col_ptrs, int *col_ids, double *col_vals, int n, int m, int *match, int *row_match, 
                    int *row_ptrs, 
                    int *row_ids,
                    double *row_vals,
                    int *fend_cols, int *fend_rows, double *thrshld, int sprankknown)

cdef extern from "./../bottled/extern/matchmaker.h":
    void old_cheap(int* col_ptrs, int* col_ids, int* match, int* row_match, int n, int m)
    void sk_cheap(int* col_ptrs, int* col_ids, int* row_ptrs, int* row_ids, int* match, int* row_match, int n, int m)
    void sk_cheap_rand(int* col_ptrs, int* col_ids, int* row_ptrs, int* row_ids, int* match, int* row_match, int n, int m)
    void mind_cheap(int* col_ptrs, int* col_ids, int* row_ptrs, int* row_ids, int* match, int* row_match, int n, int m)
    void truncrw(int* col_ptrs, int* col_ids, int* row_ptrs, int* row_ids, int* match, int* row_match, int n, int m)
    void match_dfs(int* col_ptrs, int* col_ids, int* match, int* row_match, int n, int m)
    void match_bfs(int* col_ptrs, int* col_ids, int* match, int* row_match, int n, int m)
    void match_mc21(int* col_ptrs, int* col_ids, int* match, int* row_match, int n, int m)
    void match_pf(int* col_ptrs, int* col_ids, int* match, int* row_match, int n, int m)
    void match_pf_fair(int* col_ptrs, int* col_ids, int* match, int* row_match, int n, int m)
    void match_hk(int* col_ptrs, int* col_ids, int* row_ptrs, int* row_ids, int* match, int* row_match, int n, int m)
    void match_hk_dw(int* col_ptrs, int* col_ids, int* row_ptrs, int* row_ids, int* match, int* row_match, int n, int m)
    void match_abmp(int* col_ptrs, int* col_ids, int* row_ptrs, int* row_ids, int* match, int* row_match, int n, int m)
    void match_abmp_bfs(int* col_ptrs, int* col_ids, int* row_ptrs, int* row_ids, int* match, int* row_match, int n, int m)
    void match_pr_fifo_fair(int* col_ptrs, int* col_ids, int* row_ptrs, int* row_ids, int* match, int* row_match, int n, int m, double relabel_period)
    void pr_global_relabel(int* l_label, int* r_label, int* row_ptrs, int* row_ids, int* match, int* row_match, int n, int m)
    void cheap_matching(int* col_ptrs, int* col_ids, int* row_ptrs, int* row_ids, int* match, int* row_match, int n, int m, int cheap_id)
    void matching(int* col_ptrs, int* col_ids, int* match, int* row_match, int n, int m, int match_id, int cheap_id, double relabel_period)

cdef extern from "./../bottled/mc64mwm.c":
    int mwm(double* col_vals, int* col_ids, int* col_ptrs, int* perm, int m, int n, int nz)

#wrapper copy matrix
def matching_sparse(mat):
    col_ptrs = mat.indptr.astype(np.int32,copy=False)
    col_ids = mat.indices.astype(np.int32,copy=False)
    n,m = mat.shape
    return matching_cython(col_ptrs,col_ids,n,m)

#maximum cardinality matching
def matching_cython(col_ptrs,col_ids,n,m,match_id=10,cheap_id=2,relabel_period=1.0):
    cdef int[::1] col_ptrs_memview = col_ptrs #no copy
    cdef int[::1] col_ids_memview = col_ids #no copy

    cdef int* match = <int*> PyMem_Malloc(n*sizeof(int))
    cdef int* row_match = <int*> PyMem_Malloc(m*sizeof(int))

    for i in range(m):
        row_match[i]=-1

    matching(&col_ptrs_memview[0],&col_ids_memview[0],match,row_match,n,m,match_id,cheap_id,relabel_period)

    cdef int[:] match_memview = <int[:n]> match
    cdef int[:] row_match_memview = <int[:n]> row_match

    try:
        match_array = np.asarray(match_memview,dtype=int)
        return match_array
    finally:
        PyMem_Free(match)
        PyMem_Free(row_match)

#wrapper copy matrix
def bottled_sparse(mat):
    #need copy because the C function permutes the entries of the matrix
    col_ptrs = mat.indptr.astype(np.int32,copy=True)
    col_ids = mat.indices.astype(np.int32,copy=True)
    col_vals = mat.data.astype(np.double,copy=True)
    n,m = mat.shape
    return bottled_cython(col_ptrs, col_ids, col_vals, n, m)

#bottleneck matching
def bottled_cython(col_ptrs, col_ids, col_vals, n, m):
    nz = len(col_vals)

    cdef int[::1] col_ptrs_memview = col_ptrs #no copy
    cdef int[::1] col_ids_memview = col_ids #no copy
    cdef double[::1] col_vals_memview = col_vals #no copy

    cdef int* match = <int*> PyMem_Malloc(n*sizeof(int))
    cdef int* row_match = <int*> PyMem_Malloc(m*sizeof(int))
    cdef int* row_ptrs = <int *>PyMem_Malloc(sizeof(int) * (m+1))
    cdef int* row_ids = <int *> PyMem_Malloc(sizeof(int) * nz )
    cdef double* row_vals = <double *> PyMem_Malloc(sizeof(double) * nz)
    cdef int* fend_cols = <int *> PyMem_Malloc(sizeof(int) * n)
    cdef int* fend_rows = <int *> PyMem_Malloc(sizeof(int) * m)

    cdef double thrshld = DBL_MAX

    for i in range(m):
        row_match[i]=-1

    bttlThreshold(&col_ptrs_memview[0],&col_ids_memview[0],&col_vals_memview[0],n, m,match,row_match,row_ptrs,row_ids,row_vals
    ,fend_cols, fend_rows,1,&thrshld,0)

    returned_thrshld = <double> thrshld
    
    cdef int[:] match_memview = <int[:n]> match

    try:
        match_array = np.asarray(match_memview,dtype=int)
        return match_array,returned_thrshld
    finally:
        PyMem_Free(match)
        PyMem_Free(row_match)
        PyMem_Free(row_ptrs)
        PyMem_Free(row_ids)
        PyMem_Free(row_vals)
        PyMem_Free(fend_cols)
        PyMem_Free(fend_rows)

#wrapper copy matrix
def weighted_sparse(mat):
    #need the copy, side effects from mc64
    col_ptrs = mat.indptr.astype(np.int32,copy=True)
    col_ids = mat.indices.astype(np.int32,copy=True)
    col_vals = mat.data.astype(np.double,copy=True)
    n,m = mat.shape
    return weighted_cython(col_ptrs, col_ids, col_vals, n, m)

#maximum weight matching
def weighted_cython(col_ptrs, col_ids, col_vals, n, m):
    nz = len(col_vals)

    cdef int[::1] col_ptrs_memview = col_ptrs #no copy
    cdef int[::1] col_ids_memview = col_ids #no copy
    cdef double[::1] col_vals_memview = col_vals #no copy

    cdef int* perm = <int*> PyMem_Malloc(m*sizeof(int))

    mwm(&col_vals_memview[0],&col_ids_memview[0],&col_ptrs_memview[0],perm,m,n,nz)

    cdef int[:] perm_memview = <int[:m]> perm
    
    try:
        perm_array = np.asarray(perm_memview,dtype=int)
        return perm_array
    finally:

        PyMem_Free(perm)
