import numpy as np
from scipy.sparse import csc_array

import sys
sys.path.append('./bvn')

from bottled_c import matching_sparse,bottled_sparse,weighted_sparse
import gurobipy as gp
from gurobipy import GRB
from matching import max_weight_matching



def decomp(A,nummatchings=None,myZero=1e-16,myOne=1.0,find_match='bot',objsum=0.9999):
    """
    A assumed to be doubly stochastic, and sparse csc

    A verifies in the end

    A = sum([coeff[i]*perm[i] for i in range(num)])
    """
    n,m = A.shape
    
    assert n==m
    assert find_match in ['bot','mc','mw']

    nnz = A.count_nonzero()
    T = np.abs(A)
    T.data[np.abs(T.data)<=myZero] = 0
    T.eliminate_zeros()

    sumcoeff = 0

    if not(nummatchings):
        nummatchings= n**2 - 2*n +2

    coeff=[]#contains the coefficients
    perms=[]#contains the permutation matrices
    num = 0
    while num < nummatchings and nnz>0 and sumcoeff < objsum:
        
        match find_match:
            case 'bot':
                r,alpha = bottled_sparse(T)
            case 'mc':
                r = matching_sparse(T)
                alpha = min([T[r[j],j] for j in range(n)])
            case 'mw':
                r = max_weight_matching(T)
                alpha = min([T[r[j],j] for j in range(n)])
        
        coeff.append(alpha)
        sumcoeff += alpha

        P = csc_array((np.ones(n,dtype=np.int32),(r,list(range(n)))),shape=(n,n))

        T = T - alpha*P
        perms.append(r)
        
        #delete very small entries
        T.data[np.abs(T.data)<=myZero] = 0
        T.eliminate_zeros()
        nnz = T.count_nonzero()
        
        num+=1
        
        if num%100==0:
            print(num)
            print(sumcoeff)
    
    return num,coeff,perms

def formApprx(n,coeff,perms):
    A = csc_array((n,n))
    count=0
    for i in range(len(coeff)):
        if coeff[i]<=0:#due to numerical errors, or LS
            count+=1
            continue
        A += coeff[i]* csc_array((np.ones(n,dtype=np.int32),(perms[i],list(range(n)))),shape=(n,n))
    
    minA = A.min(axis=None)
    if count>0:
        print("count negative coeffs:",count)
    assert minA >= 0
    
    return A

def build_vect_LP(T):
    nnz = T.nnz
    row,col = T.nonzero()#doesnt correspond to the order in T.data
    Tcsr = T.copy()
    Tcsr = Tcsr.tocsr()
    t = Tcsr.data
    coor = csc_array(([i for i in range(nnz)],(row,col)),shape=T.shape,dtype=int)
    return t,coor

def omp(A,nummatchings=None,myZero=None,myOne=1.0,algomp1='bot',algomp2='nnls',objsum=0.9999,vtype=GRB.CONTINUOUS):
    n,m = A.shape

    if myZero==None:
        if objsum < 1:
            eps = 1-objsum
            myZero = eps/n
            print("myZero:",myZero)
        else:
            myZero = 1e-16

    if algomp2==None:
        return decomp(A,nummatchings,myZero=myZero,myOne=myOne,find_match=algomp1,objsum=objsum)
    
    assert n==m
    assert algomp1 in ['bot','mc','mw']
    assert algomp2 in ['qp','lp']

    
    B = np.abs(A)
    B.data[np.abs(B.data)<=myZero] = 0
    B.eliminate_zeros()

    nnz = B.count_nonzero()

    sumcoeff = 0

    if not(nummatchings):
        nummatchings= n**2 - 2*n +2

    b,coord2nnz = build_vect_LP(B)

    
    (row,col) = B.nonzero()

    coeff=[]#contains the coefficients
    perms=[]#contains the permutation matrices
    num = 0

    listu = []#contains the u_i

    TOT = csc_array((n,n))
    prev = 0

    summin = 0

    need_recompute = False

    while num < nummatchings and sumcoeff < objsum:
        
        T = B - formApprx(n,coeff,perms)
        
        nnzT = T.count_nonzero()

        if nnzT == 0:
            break
        
        match algomp1:
            case 'bot':
                r,_ = bottled_sparse(T)
            case 'mc':
                r = matching_sparse(T)
            case 'mw':
                r = max_weight_matching(T)

        if -1 in r:
            print(f"Could not find perfect matching at step {num}")
        
        perms.append(r)


        match algomp2:
            case 'qp':

                if num==0:
                    M = csc_array(([1.0 for _ in range(n)],([coord2nnz[r[j],j] for j in range(n)],[0 for _ in range(n)])),shape=(nnz,1))

                    m = gp.Model("LP_OMP2")
                    m.Params.LogToConsole = 0
                    alpha = m.addMVar(shape=(1,),lb=0,vtype=vtype)
                    m.ModelSense = GRB.MINIMIZE

                    v1 = b-M@alpha
                    m.setObjective(v1@v1)

                    m.addMConstr(M,alpha, '<', b)
                else:
                    M = csc_array(([1.0 for _ in range(n)],([coord2nnz[r[j],j] for j in range(n)],[0 for _ in range(n)])),shape=(nnz,1))

                    constrs = m.getConstrs()
                    c = gp.Column()
                    
                    for j in range(n):
                        h = coord2nnz[r[j],j]
                        c.addTerms(1.0, constrs[h])

                    m.addVar(lb=0,obj=0,vtype=vtype,column=c)

                    m.update()

                    newvar = gp.MVar.fromlist([m.getVars()[-1]])
                    v2 = M@newvar
                    oldobj = m.getObjective()
                    m.setObjective(oldobj - 2*v1@v2 + v2@v2)

                    v1 = v1-v2

                m.update() 

                m.optimize()

                val = m.ObjVal
                coeff = [v.X for v in m.getVars()]
            

            case 'lp':
                
                if num==0:
                    
                    M = csc_array((nnz,1))
                    for j in range(n):
                        h = coord2nnz[r[j],j]
                        M[h,0]=1

                    m = gp.Model("LP_OMP2")
                    m.Params.LogToConsole = 0
                    alpha = m.addVars([j for j in range(num+1)],name="alpha",lb=0,vtype=vtype)
                    m.ModelSense = GRB.MAXIMIZE

                    m.setObjective(sum(alpha[j] for j in range(num+1)))
                    vect_alpha = [alpha[j] for j in range(num+1)]
                    
                    m.addMConstr(M,vect_alpha, '<=', b)
                    
                else:
                    constrs = m.getConstrs()
                    c = gp.Column()
                    
                    for j in range(n):
                        h = coord2nnz[r[j],j]
                        c.addTerms(1.0, constrs[h])

                    m.addVar(lb=0,obj=1.0,vtype=vtype,column=c)

                m.update() 
                
                m.optimize()
                val = m.ObjVal
                coeff = [v.X for v in m.getVars()]

        sumcoeff = sum(coeff)

        num+=1
        
        if num%10==0:
            print(num)
            print("sum:",sumcoeff)
    
    return num,coeff,perms

