import numpy as np
import random
from scipy.sparse import csc_array,dia_array

def scaleSK(A,maxIter=10,tol=1e-6,rtarget=None,ctarget=None):
    """
    scaling by Sinkhorn-Knopp algorithm
    returns r,c,As,numIter,err 
    """
    n,m = A.shape
    r = np.ones(n)
    c = np.ones(m)
    if not(rtarget):
        rtarget = np.ones(n)
    if not(ctarget):
        ctarget = np.ones(m)

    As = np.abs(A)

    iter = 0
    err = 1
    while iter < maxIter and err > tol:

        c = rtarget/(As.T@r)
        r = ctarget/(As@c)

        Dr = dia_array([r],offsets=[0],format="csc")
        Dc = dia_array([c],offsets=[0],format="csc")
        err = max(np.linalg.norm(Dr @ As @ c-rtarget,ord=np.inf),np.linalg.norm(Dc @ As.T @ r - ctarget,ord=np.inf))
        print(err)
        iter+=1
        if iter%100 == 0:
            print(f"iter {iter} err {err}")
    
    As = Dr @ A @ Dc
    return r,c,As,iter,err


def bnewtns(A, p=None, q=None, tol=1e-6, delta=0.1, fl=0, Delta=3,maxiter=1e3):
    
    m, n = A.shape
    if p is None:
        p = np.ones((m, 1))
    if q is None:
        q = np.ones((n, 1))
    e = np.ones((m + n, 1))
    res = []
    h = np.vstack((p, q))
    
    if fl == 1:
        print('it    in. it    res')

    g = 0.9
    etamax = 0.1
    eta = etamax
    stop_tol = tol * 0.5
    rt = tol**2
    x = e
    v = x * np.vstack((A.dot(x[m:]), A.T.dot(x[:m])))
    
    rk = h - v
    rho_km1 = np.dot(rk.T, rk)
    rout = rho_km1
    rold = rout
    MVP = 0
    i = 0

    kmaxSeen = 0

    while rout > rt and MVP < maxiter:
        i += 1
        k = 0
        y = e
        innertol = max(eta**2 * rout, rt)
        while rho_km1 > innertol:
            k += 1
            if k == 1:
                Z = rk / v
                p = Z
                rho_km1 = np.dot(rk.T, Z)
            else:
                beta = rho_km1 / rho_km2
                p = Z + beta * p
            
            tx = x * p
            w = x * np.vstack((A.dot(tx[m:]), A.T.dot(tx[:m]))) + v * p
            alpha = rho_km1 / np.dot(p.T, w)
            ap = alpha * p
            
            ynew = y + ap
            if np.min(ynew) <= delta:
                if delta == 0:
                    break
                ind = np.where(ap < 0)
                gamma = np.min((delta - y[ind]) / ap[ind])
                y = y + gamma * ap
                break
            
            if np.max(ynew) >= Delta:
                ind = np.where(ynew > Delta)
                gamma = np.min((Delta - y[ind]) / ap[ind])
                y = y + gamma * ap
                break
            
            y = ynew
            rk = rk - alpha * w
            rho_km2 = rho_km1
            Z = rk / v
            rho_km1 = np.dot(rk.T, Z)
        
        x = x * y
        v = x * np.vstack((A.dot(x[m:]), A.T.dot(x[:m])))
        rk = h - v
        rho_km1 = np.dot(rk.T, rk)
        rout = rho_km1
        MVP += k + 1
        if k > kmaxSeen:
            kmaxSeen = k
        
        rat = rout / rold
        rold = rout
        res_norm = np.sqrt(rout)
        eta_o = eta
        eta = g * rat
        if g * eta_o**2 > 0.1:
            eta = max(eta, g * eta_o**2)
        eta = min(eta, etamax)
        eta = max(eta, stop_tol / res_norm)
        
        if fl == 1:
            print('%3d %6d   %.3e %.3e %.3e %.3e' % (i, k, res_norm, np.min(y), np.max(y), np.min(x)))
            res.append(res_norm)
    
    if fl==1:
        print('Matrix-vector products for scaling= %6d, kmaxSeen = %d' % (MVP, kmaxSeen))

    return x[:m], x[m:], res, MVP

def scaleNW(A,maxIter=1000,tol=1e-6,rtarget=None,ctarget=None,sumtarget=None):

    m, n = A.shape

    if sumtarget is None:
        sumtarget = 1
    
    if rtarget is None:
        rtarget = np.ones((m, 1))*sumtarget
    if ctarget is None:
        ctarget = np.ones((n, 1))*sumtarget

    r, c, _, steps = bnewtns(A,p=rtarget,q=ctarget,tol=tol,maxiter=maxIter)
    print(f"Scaling done in {steps} steps")
    print("Objective tolerance:",tol)

    Dr = csc_array(dia_array((np.array([r.reshape(1,-1)[0]]),np.array([0])),shape=(n,n)))
    Dc = csc_array(dia_array((np.array([c.reshape(1,-1)[0]]),np.array([0])),shape=(n,n)))

    As = Dr @ A @ Dc

    m1 = np.max(np.abs(As@np.ones(n)-np.ones(n)))
    m2 = np.max(np.abs(As.T@np.ones(n)-np.ones(n)))
    # print(m1,m2)
    dev = max(m1,m2)
    print("Maximum deviation:",dev)
    return As

    
