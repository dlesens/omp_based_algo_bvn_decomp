import numpy as np
import random
from scipy.sparse import csc_array,dia_array,block_array
import scipy



#Functions for generating a doubly stochastic matrix

def random_perm(n):
    """
    generate random permutations using Fisher-Yates shuffles
    """
    p = list(range(n))
    for i in range(n-1):
        j = i+random.randint(0,n-i-1) #random integer j s.t. i <= j < n
        p[i],p[j] = p[j],p[i]   
    return p

def perm_to_matrix(p):
    """
    transforms permutation p into the corresponding permutation matrix
    """
    n = len(p)
    M = csc_array((np.array([1 for _ in range(n)]),(np.array(p),np.array(list(range(n))))),shape=(n,n))
    return M


def random_ds_matrix(n,k,distrib_alpha=None,list_alpha=None):
    """
    n is the size of the matrix
    k is the number of permutations which compose it
    distrib_alpha is the distribution of coefficients

    creates k random permutations P1..Pk, k positive alphas adding up to 1, and builds A = alpha1 * P1+...+ alpka_k * Pk
    """
    assert distrib_alpha or list_alpha
    
    if distrib_alpha:
        alpha = distrib_alpha(k)
    else:
        alpha = list_alpha
        assert len(alpha)==k
    
    sum_alpha = np.sum(alpha)

    A = csc_array((n,n))

    for i in range(k):
        A += alpha[i]/sum_alpha*perm_to_matrix(random_perm(n))
    
    return A

def special_overlap(n, k):
    # k+1 perms, one of them has n/k overlaps with each of the k others.
    # we fix the first one on the diagonal and then the others each have
    # an overlap of size n/k with the first one. We designate the overlaps
    # in a sliding window of size n/k. So the first perm matrix of the k,
    # has the first n/k entries equal to the identity. the rest is random.
    
    olapsz = int(np.ceil(n / k))
    irows = np.zeros(n * (k + 1), dtype=int)
    jcols = np.zeros(n * (k + 1), dtype=int)
    vals = np.zeros(n * (k + 1), dtype=int)

    coffs = np.zeros(k+1)

    if k <= 20:
        # for small k, let's use 2^k as in the Further notes
        coffs[0]=2**k
        coffs[1] = 1 # the second one is small.
        for ii in range(2, k + 1):
            coffs[ii] = np.ceil(2 * coffs[ii - 1])
    else:
        # coffs = np.random.randint(1, np.ceil(np.sqrt(n)) + 1, k + 1)  # k+1 numbers between 1 and sqrt(n).
        B = 2
        coffs = np.full(k+1,1)
        
        for ii in range(2, k + 1):
            for j in range(max(1,ii-B),ii):
                coffs[ii] += coffs[j]
        
        for j in range(k+1-B,k+1):
            coffs[0] += coffs[j]

    # the first one is the diagonal.
    irows[:n] = np.arange(n)  # 1 to n
    jcols[:n] = np.arange(n)  # 1 to n
    vals[:n] = np.full(n, coffs[0], dtype=int)  # each has value coffs[0]

    off = n  # an index to write the
    for ii in range(k):
        ilst = np.arange(n)  # identity
        st = ii * olapsz
        nd = min(n * k, (ii + 1) * olapsz)

        fixesi = ilst[st:nd]  # keep identity permutation
        othersi = np.setdiff1d(ilst, fixesi)  # others, with setdiff, slow surely, but correct
        sz = len(othersi)
        np.random.shuffle(othersi)  # randomly permute the non-overlapping indices for rows
        othersj = np.random.permutation(othersi)  # randomly permute the non-overlapping indices for cols
        irws = np.concatenate((fixesi, othersi))  # of size n
        jcls = np.concatenate((fixesi, othersj))  # of size n

        irows[off:off + n] = irws  # write into arrays
        jcols[off:off + n] = jcls
        vals[off:off + n] = coffs[ii + 1]
        off += n

    # build and return the matrix
    A = csc_array((vals, (irows, jcols)), shape=(n, n))
    return A,coffs
