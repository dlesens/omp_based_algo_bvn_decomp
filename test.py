import numpy as np
from scipy.sparse import csc_array
from bvn.greedy import omp,decomp
from utils.scaling import scaleNW
from utils.ds_matrix import special_overlap
from scipy.io import mmread


#Example matrix from the paper

a = 1; b=2; c=4; d=8; e=16; f=32; g=64; h=128; i=256; j=512

A = np.array([[a+b,d+i,c+h,e+j,f+g],
              [e+g,a+c,b+i,d+f,h+j],
              [f+j,e+h,d+g,b+c,a+i],
              [d+h,b+f,a+j,g+i,c+e],
              [c+i,g+j,e+f,a+h,b+d]])/(a+b+c+d+e+f+g+h+i+j)


print(A)
A = csc_array(A)

res = omp(A,nummatchings=1000,algomp1='mw',algomp2='lp',objsum=1)
print(sum(res[1]))
print(res[0],res[1])


#A matrix from the suite sparse collection
A = csc_array(mmread('ex21.mtx'))

A = np.abs(A)

n,_ = A.shape

#scaling
A = scaleNW(A,tol=1e-4,sumtarget=1)

#computation
res = omp(A,nummatchings=1000,algomp1='bot',algomp2=None,objsum=0.999)
print(sum(res[1]))
print(res[0],res[1])

#Special overlap matrix

A,coeffs = special_overlap(100,10)

res = omp(A,nummatchings=1000,algomp1='bot',algomp2='lp',objsum=sum(coeffs))
print(sum(res[1]))
print(res[0],res[1])