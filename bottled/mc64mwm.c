#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>

#include "mmio.h"
#include "matrixUtils.h"
#include <stdbool.h>

#define MAX_DOUBLE 1e100 


#define JOB_DEF 5


void  mc64id_(int *, double *);

void  mc64ad_(int *, int *, int *, int *, int *, int *,double *,
	      int *, int *, int *,int *, int *, double *,
	      int *, double *, int *);



int mwm(double* col_vals, int* col_ids, int* col_ptrs, int* perm, int m, int n, int nz)
{ 
  //int* col_vals, int* col_ids, int* col_ptrs, int* perm already allocated

  int i;
  int nicntl, ncntl, ninfo;
  int job, num, liw, ldw;
  
  int *iw;
  int *icntl, *info;
  double *dw, *cntl;

  int optScaling;
  
  /* fix mc64 control parameters */
  nicntl=10;
  ncntl=10;
  ninfo=10;
  icntl = calloc(nicntl,sizeof(int));
  info =  calloc(ninfo,sizeof(int));
  cntl = calloc(ninfo,sizeof(double));

  mc64id_(icntl,cntl);

  icntl[0] = -1;
  icntl[1] = -1;

/*allocation of mc64 workspace*/
  job = 4;
  liw = 3*n+2*m;
  ldw = 2*m+nz;

  iw = calloc(liw,sizeof(int));
  if(ldw > 0)  
    dw = calloc(ldw,sizeof(double));

  // applies absolute value
  for(i=0;i<nz;i++)
    col_vals[i] = col_vals[i]>0 ? col_vals[i] : -col_vals[i];

    //change indexation
  for(i=0;i<n+1;i++)
    col_ptrs[i] = col_ptrs[i]+1;
  
  for(i=0;i<nz;i++)
    col_ids[i] = col_ids[i]+1;

  mc64ad_(&job,&m,&n,&nz,col_ptrs,col_ids,col_vals,&num,perm,&liw,iw,&ldw,dw,icntl,cntl,info);

  //change back indexation
    for(i=0; i<m; i++){
        perm[i] = perm[i]-1;
    }

  free(icntl);
  free(info); 
  free(cntl);
  free(iw);
  if(ldw > 0)
    free(dw);
} 
